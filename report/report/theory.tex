\documentclass[report]{subfiles}
\begin{document}
\subsection{Potential Evapotranspiration}
Three general methods exist for the calculation of potential evapotranspiration (PET) in vegetated areas, namely; the Penman-Monteith equation \cite{monteith1965}, Hargreaves' equation \cite{hargreaves1985} and Thornthwaite's equation \cite{thornthwaite1948}. An outline of the required input data for each equation is shown in table~\ref{tab:ET0}. It can be seen in this table that although the Penman-Monteith is seen as the gold standard for calculating PET, and is in fact the method used by the United Nations FAO \cite{allen1998}, it requires a large number of input data points. Some of these data points can be calculated using other measurements, should they be missing, however it proved difficult to find a number of data sources, suitably distributed around Ireland, which would provide all of the required measurements. As such this work focussed only on calculations of PET using Hargreaves and Thornthwaite's equations.
\begin{table}[ht]
    \centering
    \begin{tabular}{ccc}
        \hline
            Penman-Monteith & Hargreaves & Thornthwaite\\
        \hline
        \hline
            Min. Temp. & Min. Temp & Mean Temp.\\
            Max. Temp. & Max. Temp. & Sunshine Hours\\
            Radiation & Radiation & \\
            Latitude & & \\
            Altitude &  & \\
            Avg. Vapour Pressure &  & \\
            Avg. Wind Speed at 2m &  & \\
        \hline
\end{tabular}
\caption{Necessary inputs for the three methods of calculating PET}
\label{tab:ET0}
\end{table}
\subsubsection{\r{A}ngstrom-Prescott equation}
Hargreaves' equation relies only on measurement of air temperature and solar radiation. While the air temperature is supplied directly from all of the chosen weather stations, the solar radiation is not. This can be calculated however, from the measurement of sunshine hours per day which is included in all of the datasets. To do this the \r{A}ngstrom-Prescott equation is used as follows.
\begin{equation}
    H_i = H_{0i}\left(a+b\left(\frac{s_i}{s_{0i}}\right)\right)
    \label{eq:AP}
\end{equation}
where $H_i$ is the monthly solar radiation in $MJ~m^{-2}day^{-1}$, $s_i$ is the total monthly sunlight in hours.
$H_{0i}$ \& $s_{0i}$, the extraterrestrial radiation \& daily maximum possible hours are defined as follows \cite{keawsang2018}:
\begin{equation}
    H_{0i} = \frac{24I_{sc}}{\pi}\left(1+0.\cos\left(\frac{360(i)}{365}\right)\right)\left(\cos(\phi)\cos(\delta_i)\sin(\omega_{si})+\frac{2\pi\omega_{si}}{360}\sin(\phi)\sin(\delta_i)\right)
\end{equation}
\begin{equation}
    S_{0i} = \frac{2\omega_{si}}{15}
\end{equation}
where the sunset solar hour angle is given by \cite{keawsang2018}:
\begin{equation}
    \omega_{si} = \cos^{-1}(-\tan(\phi)\tan(\delta_i))
    \label{eq:omega}
\end{equation}
and the solar declination angle is given as follows \cite{decl}:
\begin{equation}
    \delta_i = -23.45\cos\left(\frac{360}{365}(i+10)\right)
    \label{eq:delta}
\end{equation}
The coefficients $a$ \& $b$ in eq.~\ref{eq:AP} are calculated by \citeasnoun{ishola2019}.\\
This value of $H_0$ is given in $MJ~m^{-2}day^{-1}$, for the final calculation of the Hargreaves equation it will need to be converted to equivalent water evaporation over time ($mm~day^{-1}$) as follows \cite{faoAnnex}:
\begin{align}
    1~mm~day^{-1} &= 2.45~MJ~m^{-2}day^{-1}\\
    R_a &= \frac{H_i}{2.45}
\end{align}
\subsubsection{Haregreaves' equation}
The calculated value for solar radiation ($R_a$) can then be used along with the daily minimum and maximum air temperatures to calculate the rate of PET given here as $ET_0$.
\begin{equation}
    ET_0 = 2.3\times10^{-3}R_a\left(\frac{T_{max}+T_{min}}{2}+17.8\right)\sqrt{T_{max}-T_{min}}
    \label{eq:HG}
\end{equation}
\subsubsection{Thornthwaite's equation}
Thornthwaite's equation requires even fewer inputs with no prior calculation necessary. 
\begin{equation}
    ET_0 = 16 \left(\frac{s_i}{12}\right) \left(\frac{N}{30}\right) \left(\frac{10T_d}{I}\right)^{\alpha}
    \label{eq:TH}
\end{equation}
Here again $ET_0$ is the estimated PET ($mm/month$), $T_d$ is the average daily temperature (degrees Celsius; if this is negative, use $0$) of the month being calculated, $N$ is the number of days in the month being calculated, $s_i$ is the average day length (hours) of the month being calculated. All of the required data here is available directly from the datasets.

\begin{equation}
    \alpha = (6.75 \times 10^{-7}) I^3 - (7.71 \times 10^{-5}) I^2 + (1.792 \times 10^{-2}) I + 0.49239
\end{equation}
\begin{equation}
    I = \sum_{i=1}^{12} \left(\frac{T_{m_{i}}}{5}\right)^{1.514}
\end{equation}
is a heat index which depends on the 12 monthly mean temperatures $T_{m_{i}}$.
\subsubsection{Python Implementation}
A convenient python library, PyETo has gathered all of these equations into a simple set of methods \cite{pyeto}.\\
In order to make predictions of the estimated rate of PET it will be necessary to undertake some time series analysis (TSA) of the input variables for each equation. Several python libraries exist to perform this function and after some research the statsmodel library was chosen \cite{statsmodel}. The TSA was implemented using the statsmodel's Autoregressive Integrated Moving Average or ARIMA method. This model allows estimating parameters by various methods and is suitable for use in including prediction / forecasting, residual diagnostics, simulation and impulse responses, etc. \cite{statsmodel}.\\
The forecast values would then need to be utilised to predict the rate of PET . This would be done following the work of \citeasnoun{feng2017}, \citeasnoun{patil2016} \& \citeasnoun{mohammadi2015} by using a generalised regression neural network (GRNN) to predict the future rates of PET. The GRNN in this case was provided by the NeuPy package \cite{neupy}. The topology of the network is shown in fig.~\ref{fig:grnn_top}.
\begin{figure}[ht]
    \vspace{10pt}
    \centering
    \includegraphics[width=0.55\linewidth]{../images/GRNN_topology.png}
    \caption{The topology of the GRNN.}
    \label{fig:grnn_top}
\end{figure}
\subsubsection{Statistical Analysis}
For analysis of the results generated Pearson's Correlation Coefficient test (PCC) was chosen following the work of \citeasnoun{colston2018} where they applied it to the analysis of meteorological data. This is a common test for measuring the linear dependence between two variables. It returns a value between $[-1, 1]$ with $1$ meaning perfect correlation between the variables \cite{kirkwood2010}. The correlation coefficient is given by eq.~\ref{eq:corr}
\begin{equation}
    r = \frac{\sum (x-x_m)(y-y_m)}{\sqrt{\sum (x-x_m)^2\sum (y-y_m)^2}}
    \label{eq:corr}
\end{equation}
Where $x_m$ \& $y_m$ are the mean values of the vectors $x$ \& $y$.
\end{document}
